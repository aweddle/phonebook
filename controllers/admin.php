<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/5/13
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */
class Admin extends Admin_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();

        //Loading required classes

        $this->lang->load('phone');
        $this->load->model('phonebook_m');
        $this->load->helper('form');
        $this->load->library('form_validation');


        //DB Tables
        $this->_contacts = 'phonebook_contacts';

        //Redirects
        $this->_redirect = 'admin/phonebook/';

    }
    public function index()
    {
        $this->template->title($this->module_details['name'])
            ->build('index', $this->data);
    }
    public function add()
    {
        $this->template->title($this->module_details['name'])
            ->append_js('module::admin/add.js')
            ->build('admin/add', $this->data);
    }
    public function view()
    {
        $this->data->info = $this->phonebook_m->view();  //Some sort of error here
        $this->template->title($this->module_details['name'])
            ->append_js('module::admin/view.js')
            ->build('admin/view', $this->data);
    }

    public function alpha_num_dash_space($str)
    {
        return ( ! preg_match("/^([-a-z0-9_ ])+$/i", $str)) ? FALSE : TRUE;
    }
    public function alpha_dash_space($str)
    {
        return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    }
    public function ajax_edit()
    {
        $id = $this->input->post('id');

        if($this->form_validation->run('add') == true)
        {
            unset($_POST['id']);
            $this->phonebook_m->edit($id, $_POST);
            echo json_encode(array('status'=>true, 'message'=>"Successfully updated contact!"));
        }
        else
        {
            $contact = $this->phonebook_m->get($id)->row_array();
            echo json_encode(array('status'=>false, 'message'=> validation_errors(), "contact" => $contact));
        }

    }
    public function ajax_delete()
    {
        $id = $this->input->post('id');
        $this->phonebook_m->delete($id);
        echo true;
    }
    public function ajax_add()
    {
        if ($this->form_validation->run('add'))
        {
            $this->phonebook_m->add($_POST);
            echo json_encode(array('status'=>201, 'message' => "Successfully added contact!"));
        }
        else
        {
            echo validation_errors();
        }
    }
    public function ajax_favorite()
    {
        $id = $this->input->post('id');
        $this->phonebook_m->delete_favorite($id);
    }
    public function favorite()
    {
       $this->data->info = $this->phonebook_m->get_favorite();
        $this->template->title($this->module_details['name'])
            ->append_js('module::admin/view.js')
            ->build('admin/favorite', $this->data);
    }
}
    //    public function moreinfo($id)
//    {
    //Unnecessary code


//        if(isset($_POST['submitted']) && $this->form_validation->run('add'))
//        {
//            $id = $this->input->post('id');
//            unset($_POST['id']);
//            $this->phonebook_m->edit($id, $this->_modify_post($_POST));
//        }
//        $this->data->info = $this->phonebook_m->more_info($id);  //Same error as above. Must not like passing info
//        $this->template->title($this->module_details['name'])
//            ->build('admin/moreinfo', $this->data);
//    }
//    public function delete($id)
//    {
    //Unnecessary code

//        $this->phonebook_m->delete($id);
//        $this->data->info = $this->phonebook_m->drop_pop();  //Some sort of error here
//        $this->template->title($this->module_details['name'])
//            ->build('admin/view', $this->data);
//    }
//    public function editset($id)
//    {
    //Unnecessary code now

//        $this->data->info = $this->phonebook_m->more_info($id)->row();
//        $this->template->title($this->module_details['name'])
//            ->append_js('module::view.js')
//            ->build('admin/editinfo', $this->data);
//    }

//    private function _modify_post($post)
//    {
//        unset($post['btnAction']);
//        unset($post['submitted']);
//        unset($post['lang']);
//        return $post;
//    }


