<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Phonebook extends Public_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();

        //Loading required classes
        $this->lang->load('phone');
        $this->load->model('phonebook_m');
        $this->load->helper('form');
        $this->load->library('form_validation');

        //DB Tables
        $this->_contacts = 'phonebook_contacts';

        //Redirects
        $this->_redirect = 'admin/phonebook/';
    }

    public function view()
    {
        $this->data->info = $this->phonebook_m->view();
        $this->template->title($this->module_details['name'])
            ->append_js('module::public/view.js')
            ->build('public/view', $this->data);
    }
    public function favorite()
    {
        $this->data->info = $this->phonebook_m->get_favorite();
        $this->template->title($this->module_details['name'])
            ->build('public/favorite', $this->data);
    }
    public function view_more()
    {
        $id = $this->input->post('id');
        $this->data->info = $this->phonebook_m->get($id);
        $this->template->title($this->module_details['name'])
            ->build('public/view_more', $this->data);
    }

}