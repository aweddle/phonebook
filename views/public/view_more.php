<div class="block well">
    <div class="navbar">
        <div class="navbar-inner"><h5>Table classes</h5></div>
    </div>
    <div class="table-overflow">
        <table class="table table-bordered table-striped table-block">
            <thead>
            <?php foreach($info->result() as $row): ?>
            <tr>
                <th><?php echo "Name"; ?></th>
                <td><?php echo $row->name; ?></td>
            </tr>
            <tr>
                <th><?php echo "Phone"; ?></th>
                <td><?php echo $row->phone;?></td>
            </tr>
            <tr>
                <th><?php echo "Email"; ?></th>
                <td><?php echo $row->email;?></td>

            </tr>
            </thead>


            <?php endforeach; ?>

        </table>
    </div>
</div>