<div class="block well">
    <div class="navbar">
        <div class="navbar-inner"><h5>Contacts</h5></div>
    </div>
    <div class="table-overflow">
        <table class="table table-bordered table-striped table-block">
            <thead>
            <tr>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Phone"; ?></th>
                <th><?php echo "Email"; ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($info->result() as $row): ?>

                <tr id="<?php echo $row->id;?>" class='rclick'>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->phone;?></td>
                    <td><?php echo $row->email;?></td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>