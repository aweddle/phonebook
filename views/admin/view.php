<div id="error">
</div>
<div>
    <section class='title'>
        <h4><?php echo "All Contacts"; ?></h4>
    </section>
<section>
    <div>
        <table border="0">
            <thead>
            <tr>
                <th><?php echo "Favorite"; ?></th>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Phone"; ?></th>
                <th><?php echo "Email"; ?></th>
                <th><?php echo "Address"; ?></th>
                <th><?php echo "City"; ?></th>
                <th><?php echo "State"; ?></th>
                <th><?php echo "Zip"; ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php echo form_open(); ?>
            <?php foreach($info->result() as $info): ?>
            <tr id="<?php echo $info->id;?>">
                <td><?php echo form_checkbox(array('name' => 'favorite', 'value' => $info->favorite, 'checked'=>$info->favorite, 'class' =>'favorite'));?></td>
                <td><?php echo form_input(array('name'=>'name', 'value'=> $info->name, "class"=>"name")); ?></td>
                <td><?php echo form_input(array('name'=>'phone', 'value'=>$info->phone, 'class'=>'phone', 'maxlength' => '10', 'size' => '10'));?></td>
                <td><?php echo form_input(array('name'=>'email', 'value'=>$info->email, 'class'=>'email'));?></td>
                <td><?php echo form_input(array('name'=>'address', 'value'=>$info->address, 'class'=>'address'));?></td>
                <td><?php echo form_input(array('name'=>'city', 'value'=>$info->city, 'class'=>'city'));?></td>
                <td><?php echo form_input(array('name'=>'state', 'value'=>$info->state, 'class'=>'state', 'maxlength' => '2', 'size' => '2'));?></td>
                <td><?php echo form_input(array('name'=>'zip', 'value'=>$info->zip, 'class'=>'zip', 'maxlength' => '10', 'size' => '10'));?></td>
                <td><?php echo form_button(array('name' => 'edit', 'content'=>'Edit', 'class'=>'edit')); ?>
                <?php echo form_button(array('name' => 'delete', 'content'=>'Delete', 'class'=>'delete')); ?>
                <?php echo form_button(array('name' => 'submit', 'content'=>'Submit', 'class'=>'submit')); ?></td>
             </tr>
            <?php endforeach; ?>
            </tbody>
            <?php echo form_close(); ?>
        </table>
    </div>
</section>
</div>

