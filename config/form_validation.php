<?php if(!defined('BASEPATH')) exit("No direct script access allowed");

$config = array(
    'add' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|callback_alpha_dash_space',
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'required',
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email|trim',
        ),
        array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'required|callback_alpha_num_dash_space',
        ),
        array(
            'field' => 'city',
            'label' => 'City',
            'rules' => 'required|alpha_dash_space',
        ),
        array(
            'field' => 'state',
            'label' => 'State',
            'rules' => 'required|alpha|max_length[2]|min_length[2]',
        ),
        array(
            'field' => 'zip',
            'label' => 'Zip',
            'rules' => 'required|numeric|min_length[5}',
        )
    )
);