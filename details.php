<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/5/13
 * Time: 10:29 AM
 * To change this template use File | Settings | File Templates.
 */

class Module_Phonebook extends Module
{
    public $version ='1.1';

    public function info()
    {
        $info = array(
            'name' => array(
                'en' => 'Phonebook'
            ),
            'description' => array(
                'en' => 'This module stores and displays contact information.'
            ),
            'frontend' => true,
            'backend' => true,
            'sections' => array(
                'Phonebook' => array(
                    'name' => 'phonebook:phonebook',
                    'uri' => 'admin/phonebook',
                ),
                'Add Contact' => array(
                    'name' => 'phonebook:add',
                    'uri' => 'admin/phonebook/add/'
                ),
                'View Contacts' => array(
                    'name' => 'phonebook:view',
                    'uri' => 'admin/phonebook/view/'
                ),
                'View Favorites' => array(
                    'name' => 'phonebook:favorite',
                    'uri' => 'admin/phonebook/favorite'
                ),
            )
         );
        // Legacy Support
        if ( CMS_VERSION < '2.2.0' )
        {
            $info['frontend'] = true;
            $info['backend'] = true;
            $info['menu']    = 'Phonebook';
        }
        return $info;
    }

    public function install()
    {
        $this->dbforge->drop_table('phonebook_contacts');
        $this->db->delete('settings', array('module' => 'phonebook'));

        $tables = array(
            'phonebook_contacts' => array(
                'id'        => array('type' => 'INT',     'constraint' => '11', 'primary' => true, 'auto_increment' => true),
                'favorite'  => array('type' => 'BOOLEAN'),
                'name'      => array('type' => 'VARCHAR', 'constraint' => '50'),
                'phone'     => array('type' => 'VARCHAR', 'constraint' => '10'),
                'email'     => array('type' => 'VARCHAR', 'constraint' => '50'),
                'address'   => array('type' => 'VARCHAR', 'constraint' => '100'),
                'city'      => array('type' => 'VARCHAR', 'constraint' => '25'),
                'state'     => array('type' => 'VARCHAR', 'constraint' => '2'),
                'zip'       => array('type' => 'VARCHAR', 'constraint' => '10')
                )
            );
        if(!$this->install_tables($tables))
        {
            return false;
        }

        return true;
    }
    public function uninstall()
    {
        $this->dbforge->drop_table('phonebook_contacts');

        $this->db->delete('settings', array('module' => 'phonebook'));
        {
            return true;
        }
    }
    public function upgrade($old_version)
    {
        if($old_version <= '1.0')
        {
       $fields = array(
           'city' => array('type' => 'VARCHAR', 'constraint' => '25'));
        $this->dbforge->add_column('phonebook_contacts', $fields);
        }
    }
    public function help()
    {
        //Help information goes here
        return "No help documentation has been added for this module";
    }
    //Nav Menu
    public function admin_menu(&$menu)
    {
        if(isset($menu['Phonebook']))
        {
            $menu['Phonebook']['Phonebook'] = 'admin/phonebook';
        }
        else
        {
            $menu['Phonebook'] = array(
                'Phonebook' => 'admin/phonebook'
            );
        };
    }
}