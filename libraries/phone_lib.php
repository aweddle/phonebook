<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class phone_lib
{
    public $data;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function build($page, $data)
    {
        if($data != null)
        {
            $this->ci->template->title($this->ci->module_details['name'])
                ->build($page, $data);
        }
        else
        {
            $this->ci->template->title($this->ci->module_details['name'])
                ->build($page);
        }
    }
}