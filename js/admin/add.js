/**
 * Created with JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/14/13
 * Time: 11:01 AM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){

//    $('.favorite').on('click', function(){
//        if($('.favorite').prop('checked') == true)
//        {
//            $('.favorite').attr('value', 1);
//        }
//        else
//        {
//            $('.favorite').attr('value', 0);
//        }
//    })

    $('.add').on('click', function(){


        //Checks to see if checkbox ix checked, returns value of 1 for true, 0 for false, and passes it to post
        var isChecked = $('.favorite').attr('checked')?1:0;


        $.ajax({
            url: '/admin/phonebook/ajax_add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                favorite:  isChecked,
                name:      $(".name").val(),
                phone:     $(".phone").val(),
                email:     $(".email").val(),
                address:   $(".address").val(),
                city:      $(".city").val(),
                state:     $(".state").val(),
                zip:       $(".zip").val()
            },
            success: function(output)
            {
             var string= '';
                string+='<div class="alert success animated fadeIn">' + output.message + '</div>';
                $('#error').html('').prepend(string);
            },
            error: function (output)
            {
                var string = '';
                string+='<div class="alert error animated fadeIn">' + output.responseText + '</div>';
                $('#error').html('').prepend(string);
            }
        });
        var tr = $(this).closest('tr');
        tr.find(":text").attr('value', null);
    });
 });