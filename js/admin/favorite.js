/**
 * Created with JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/15/13
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){

    //Disable all input fields

    $('.favorite').attr('disabled', true);
    $('.name').attr('disabled', true);
    $('.phone').attr('disabled', true);
    $('.email').attr('disabled', true);
    $('.address').attr('disabled', true);
    $('.city').attr('disabled', true);
    $('.state').attr('disabled', true);
    $('.zip').attr('disabled', true);



    $('.delete').on('click', function(){

        var tr = $(this).closest('tr');


        $.ajax({
            url: '/admin/phonebook/ajax_favorite',
            type: 'POST',
            dataType: 'json',
            data:
            {
                id:        tr.attr('id')
            }

        });
        $(this).closest('tr').remove();
    });

});