/**
 * Created with JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/12/13
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */



$(document).ready(function(){

    //Hide submit button and disable all input fields

    $(".submit").hide();

    $('.favorite').attr('disabled', true);
    $('.name').attr('disabled', true);
    $('.phone').attr('disabled', true);
    $('.email').attr('disabled', true);
    $('.address').attr('disabled', true);
    $('.city').attr('disabled', true);
    $('.state').attr('disabled', true);
    $('.zip').attr('disabled', true);



    $('.edit').on('click', function(){


        // Buttons
        //Hides Edit and Delete buttons to show Submit button
        $(this).hide();
        $(this).siblings('.submit').show();
        $(this).siblings('.delete').hide();


        // Enables text boxes to be changed
        var tr = $(this).closest('tr');
        tr.find(":input").attr('disabled', false);

    });

    $('.submit').on("click",  function(){


        //Buttons
        //Hides submit, shows edit and delete
        $(this).hide();
        $(this).siblings('.edit').show();
        $(this).siblings('.delete').show();

        //Disables text boxes again
        var tr = $(this).closest('tr');
        tr.find(":input").attr('disabled', true);


        ////Checks to see if checkbox ix checked, returns value of 1 for true, 0 for false, and passes it to post
        var isChecked = tr.find('.favorite').attr('checked')?1:0;

        $.ajax({
            url: '/admin/phonebook/ajax_edit',
            type: 'POST',
            dataType: 'json',
            data:
            {
                id:        tr.attr('id'),
                favorite:  isChecked,
                name :     tr.find(":input[name=name]").val(),
                phone:     tr.find(":input[name=phone]").val(),
                email:     tr.find(":input[name=email]").val(),
                address:   tr.find(":input[name=address]").val(),
                city:      tr.find(":input[name=city]").val(),
                state:     tr.find(":input[name=state]").val(),
                zip:       tr.find(":input[name=zip]").val()
            },
            success: function(output)
            {
<<<<<<< HEAD:js/view.js
                if (output.status == true)
                {
=======
                if(output.status == true)
                {
                    //Displays string if edit is successful
>>>>>>> prod/1.1:js/admin/view.js
                    var string= '';
                    string+='<div class="alert success animated fadeIn">' + output.message + '</div>';
                    $('#error').html('').prepend(string);
                }
                else
                {
<<<<<<< HEAD:js/view.js
                    alert(output.contact.name);
                    var string = '';
                    string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
                    $('#error').html('').prepend(string);
                    tr.find(":input[name=name]").attr('value', output.contact.name);
                    tr.find(":input[name=phone]").attr('value', output.contact.phone);
                    tr.find(":input[name=email]").attr('value', output.contact.email);
                    tr.find(":input[name=address]").attr('value', output.contact.address);
                    tr.find(":input[name=zip]").attr('value', output.contact.zip);
                    tr.find(":input[name=state]").attr('value', output.contact.state);

                }




            }
//            error: function (output)
//            {
////                console.log(output);
//                var string = '';
//                string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
//                $('#error').html('').prepend(string);
//                tr.find(":text['name']").attr('value', output.contact.name);
//                tr.find(":text['phone']").attr('value', output.contact.phone);
//                tr.find(":text['email']").attr('value', output.contact.email);
//                tr.find(":text['address']").attr('value', output.contact.address);
//                tr.find(":text['state']").attr('value', output.contact.state);
//                tr.find(":text['zip']").attr('value', output.contact.zip);
//
//
//            }


=======
                    //Displays form validation error if edit is unsuccessful and reverts back to original values
                    var string = '';
                    string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
                    $('#error').html('').prepend(string);
                    tr.find(":input[name=favorite]").val(output.contact.favorite);
                    tr.find(":input[name=name]").val(output.contact.name);
                    tr.find(":input[name=phone]").val(output.contact.phone);
                    tr.find(":input[name=email]").val(output.contact.email);
                    tr.find(":input[name=address]").val(output.contact.address);
                    tr.find(":input[name=city]").val(output.contact.city);
                    tr.find(":input[name=state]").val(output.contact.state);
                    tr.find(":input[name=zip]").val(output.contact.zip);
                }
            }
>>>>>>> prod/1.1:js/admin/view.js
        });
    });


    $('.delete').click(function(){

        var tr = $(this).closest('tr');
        tr.find(":text").attr('disabled', true);

        $.ajax({
            url: '/admin/phonebook/ajax_delete',
            type: 'POST',
            dataType: 'json',
            data:
            {
                id:        tr.attr('id')
            }
    });
        //Makes current table row disappear after deletion from table
        $(this).closest('tr').remove();
    });
});


//   Early form submit attempt
//    $("#update_form").submit(function()
//    {
//
//
//        $.ajax({
//
//            url: '/admin/phonebook/ajax_edit',
//            type: 'POST',
//            dataType: 'json',
//            data:
//            {
//                id:        $("#id").val(),
//                name :     $(".name").val(),
//                phone:     $(".phone").val(),
//                email:     $(".email").val(),
//                address:   $(".address").val(),
//                state:     $(".state").val(),
//                zip:       $(".zip").val()
//            }
//
//        });
//
//    });

//Early disable input on button click
//        $('.name').attr('disabled', true);
//        $('.phone').attr('disabled', true);
//        $('.email').attr('disabled', true);
//        $('.address').attr('disabled', true);
//        $('.state').attr('disabled', true);
//        $('.zip').attr('disabled', true);
//    });


//Early enable input on button click
//        $('.name').attr('disabled', false);
//        $('.phone').attr('disabled', false);
//        $('.email').attr('disabled', false);
//        $('.address').attr('disabled', false);
//        $('.state').attr('disabled', false);
//        $('.zip').attr('disabled', false);

