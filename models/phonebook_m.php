<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/5/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
class Phonebook_m extends MY_Model
{
    public function __construct()
    {
        //Table aliases
        $this->_contacts = "phonebook_contacts";
    }
    public function add($params)
    {
        $this->db->insert($this->_contacts, $params);
    }
    public function view()
    {

        return $this->db->get($this->_contacts);
    }
    public function get($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_contacts);
    }
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_contacts);
    }
    public function edit($id, $params)
    {
        $this->db->where('id', $id);
        $this->db->update($this->_contacts, $params);
    }
    public function get_favorite()
    {
        $this->db->where('favorite', 1);
        return $this->db->get($this->_contacts);
    }
    public function delete_favorite($id)
    {
        $data = array('favorite' => 0);
        $this->db->where('id', $id);
        $this->db->update($this->_contacts, $data);
    }

}

//    public function more_info($id)
//    {
//Unnecessary Code
//
//        return $this->db->get_where($this->_contacts, array('id' => $id));
//    }

